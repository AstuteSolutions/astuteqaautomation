class DemoPage
  include PageObject
  def connect_to_application(application) #this connects to the web application based off of the Scenario Example |application|
    case (application)
    when 'Google'
      browser.goto('https://www.google.com/')
      browser.driver.manage.window.maximize
    when 'Github'
      browser.goto('https://www.github.com/')
      browser.driver.manage.window.maximize
    else
      puts 'ERROR! no application matches: ' + application.to_s
    end
    sleep 5
  end

  def validate_application(url) #this validates that the application URL matches the Scenario Example |url|
    browser.url.include?(url).should ==true
  end
end