Feature: Demo

  Scenario Outline: Connect to Web Application
    Given I connect to "<application>"
    Then I should see "<url>"

    Examples:
      | notes                | application | url                     |
      | Connecting to Google | Google      | https://www.google.com/ |
      | Connecting to Yahoo  | Github      | https://github.com/     |