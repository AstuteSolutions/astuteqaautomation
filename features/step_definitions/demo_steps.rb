Given(/^I am on the Internet$/) do
  @demo_page
end

Given(/^I connect to "([^"]*)"$/) do |application|
  @demo_page.connect_to_application(application)
end

Then(/^I should see "([^"]*)"$/) do |url|
  @demo_page.validate_application(url)
end